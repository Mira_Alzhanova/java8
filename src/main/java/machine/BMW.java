package machine;

public class BMW extends Car {
    private final String model;

    public BMW(int horsePowers, String serialNumber, String color) {
        super(horsePowers,serialNumber,color);
        model = "BMW";
    }

    public String getModel() {
        return model;
    }
}
