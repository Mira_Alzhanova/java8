package machine;

public class Car implements Machine, Executable{
    private int horsePowers;
    private String serialNumber;
    private String color;

    public Car(int horsePowers, String serialNumber, String color) {
        this.horsePowers = horsePowers;
        this.serialNumber = serialNumber;
        this.color = color;
    }

    public int getHorsePowers() {
        return horsePowers;
    }
    public void setHorsePowers(int horsePowers) {
        this.horsePowers = horsePowers;
    }

    public String getSerialNumber() {
        return serialNumber;
    }
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getColor(){
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public void move() {
        System.out.println("Moving");
    }

    @Override
    public void stop() {
        System.out.println("Stop");
    }

    @Override
    public void execute() {
        System.out.println("Execute");
    }
}
