package machine;

public class Opel extends Car implements Executable{
    public final String model;

    public Opel(int horsePowers, String serialNumber, String color) {
        super(horsePowers, serialNumber, color);
        model = "Opel";
    }

    public String getModel() {
        return model;
    }

    @Override
    public void execute() {
        super.execute();
    }
}
