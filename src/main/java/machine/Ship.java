package machine;

public class Ship implements Machine {
    public int amountOfMast;
    public String serialNumber;

    public Ship(int amountOfMast, String serialNumber) {
        this.amountOfMast = amountOfMast;
        this.serialNumber = serialNumber;
    }

    public int getAmountOfMast() {
        return amountOfMast;
    }

    public void setAmountOfMast(int amountOfMast) {
        this.amountOfMast = amountOfMast;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Override
    public void move() {
        System.out.println("Moving");
    }

    @Override
    public void stop() {
        System.out.println("Stop");
    }
}
