import machine.Car;
import machine.Executable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class Reflections {
    public static int amountOfCarClassInArray(Object[] objects){//1
        int count = 0;
        for(Object o: objects){
            if(o instanceof Car){
                count++;
            }
        }
        return count;
    }

    public static List<String> getMethodsForObject(Object object){//2
        List<String> res = new ArrayList<>();
        Class<?> objectClass = object.getClass();
        Method[] objectMethods = objectClass.getDeclaredMethods();
        for (Method m: objectMethods){
            if(Modifier.isPublic(m.getModifiers())){
                res.add(m.getName());
            }
        }
        return res;
    }

    public static List<String> getAllSuperClasses(Object object){
        List<String> res = new ArrayList<>();
        Class<?> temp = object.getClass();
        while(!temp.equals(Object.class)){
            temp = temp.getSuperclass();
            res.add(temp.getSimpleName());
        }
        return res;
    }

    public static int getAmountOfExecObjects(Object[] objects) throws InvocationTargetException, IllegalAccessException {//4
        int count = 0;
        for (Object obj: objects){
            for(Class<?> inter: obj.getClass().getInterfaces()){
                if(inter.equals(Executable.class)){
                    for (Method met: obj.getClass().getDeclaredMethods()){
                        if (met.getName().equals("execute")){
                            count++;
                            met.invoke(obj);
                        }
                    }
                }
            }
        }
        return count;
    }

    public static List<String> getSettersList(Object object){//5
        List<String> res = new ArrayList<>();
        Class<?> objectClass = object.getClass();
        Method[] objectMethods = objectClass.getDeclaredMethods();
        for (Method m: objectMethods){
            if(m.getReturnType() == void.class && !Modifier.isStatic(m.getModifiers()) &&
                    Modifier.isPublic(m.getModifiers()) && m.getName().startsWith("set") && m.getParameterCount() == 1){
                res.add(m.getName());
            }
        }
        return res;
    }

    public static List<String> getGettersList(Object object){//5
        List<String> res = new ArrayList<>();
        Class<?> objectClass = object.getClass();
        Method[] objectMethods = objectClass.getDeclaredMethods();
        for (Method m: objectMethods){
            if(m.getReturnType() != void.class && !Modifier.isStatic(m.getModifiers()) &&
                    Modifier.isPublic(m.getModifiers()) && m.getName().startsWith("get")){
                res.add(m.getName());
            }
        }
        return res;
    }
}
