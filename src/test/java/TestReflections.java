import machine.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class TestReflections {
    @Test
    public void testAmountOfCarClassInArray(){
        Object[] objects = new Object[]{
                new Car(500, "1234", "Green"),
                new BMW(30, "jg", "Black"),
                new Yacht(33, "44343j"),
                3, 55.0
        };

        Assert.assertEquals(2, Reflections.amountOfCarClassInArray(objects));
    }

    @Test
    public void testGetMethodsForObject(){
        Car car = new Car(500, "1234", "green");

        for (String s: Reflections.getMethodsForObject(car)){
            System.out.println(s);
        }
    }

    @Test
    public void testGetAllSuperClasses() throws IOException {
        BMW_E34 bmw = new BMW_E34(30, "jg", "Black");
        for (String s: Reflections.getAllSuperClasses(bmw)){
            System.out.print(" -> " + s);
        }
    }

    @Test
    public void testGetAmountOfExecObjects() throws InvocationTargetException, IllegalAccessException {
        Object[] objects = new Object[]{
                //new Car(500, "1234", "Green"),
                new BMW(30, "jg", "Black"),
                new Opel(55, "jjj3", "Blue"),
                new Yacht(33, "44343j"),
                3, 55.0
        };

        Assert.assertEquals(1, Reflections.getAmountOfExecObjects(objects));
    }

    @Test
    public void testGetSettersList(){
        Car car = new Car(500, "1234", "green");

        for (String s: Reflections.getSettersList(car)){
            System.out.println(s);
        }
    }

    @Test
    public void testGetGettersList(){
        Car car = new Car(500, "1234", "green");

        for (String s: Reflections.getGettersList(car)){
            System.out.println(s);
        }
    }


}
